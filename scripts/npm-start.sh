source /home/ec2-user/.bash_profile

#!/usr/bin/env bash
set -e

cd /home/ec2-user/sample
pm2 start server.js -f
